// -*-C++-*-

#ifndef VECMATHLIB_H
#define VECMATHLIB_H

#if defined VML_DEBUG || defined VML_NODEBUG
#  if defined VML_DEBUG && defined VML_NODEBUG
#    error "Only one of VML_DEBUG or VML_NODEBUG may be defined"
#  endif
#else
// default
#  define VML_DEBUG
#endif

#undef VML_HAVE_DENORMALS
#undef VML_HAVE_INF
#undef VML_HAVE_NAN
#undef VML_HAVE_SIGNED_ZERO



#include <cassert>

#ifdef VML_DEBUG
#  define VML_ASSERT(x) assert(x)
#else
#  define VML_ASSERT(x) ((void)0)
#endif

// Scalarise all vector operations, and use libm's functions
#include "vec_pseudo.h"

// Scalarise all vector operations; don't use libm,, use only VML's
// functions
#include "vec_test.h"

// Intel SSE 2
#if defined __SSE2__
#  include "vec_float_sse2_scalar.h"
#  include "vec_float_sse2.h"
#endif
// Intel AVX
#if defined __AVX__
#  include "vec_float_avx.h"
#endif

// Intel SSE 2
#if defined __SSE2__
#  include "vec_double_sse2_scalar.h"
#  include "vec_double_sse2.h"
#endif
// Intel AVX
#if defined __AVX__
#  include "vec_double_avx.h"
#endif

#endif // #ifndef VECMATHLIB_H
